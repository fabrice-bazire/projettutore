$(function() {

    $("#connexion").click(refreshUserList);

    $("#create").click(creerCompte);

    $("#reinitialiser").click(Reinitialisation);

    function Reinitialisation(){
        console.log($("#emailreinitialiser").val());
        var expressionReguliere = /^(([^<>()[]\.,;:s@]+(.[^<>()[]\.,;:s@]+)*)|(.+))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/;
        if (expressionReguliere.test($("#emailreinitialiser").val())){
            confirm("Un mail va vous être envoyé pour réinitialiser votre mot de passe");
            window.location = "Connexion.html";
        }else{
            confirm("Votre mail n'est pas valide, veuillez réessayer");
        }
    }

    function creerCompte(){
        window.location = "User.html";
    }

    function refreshUserList(){
        console.log("coucou");
        requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/user";
        fetch(requete)
        .then( response => {
                  if (response.ok) return response.json();
                  else throw new Error('Problème ajax: '+response.status);
                }
            )
        .then(verifierUser)
        .catch(onerror);
      }

      function onerror(err) {
        $("#lescategories").html("<b>Impossible de récupérer les utilisateurs !</b>"+err);
    }

    function verifierUser(user) {
        console.log("bonjour");
        console.log(user);
        for (let i=0 ; i <user['user'].length; i++){
            console.log(user['user'][i].email);
            console.log($("#email").val());
            if (user['user'][i].email == $("#email").val()){
                console.log("email ok");
                if (user['user'][i].mdp == $("#password").val()){
                    window.location = "Produit.html";
                }
            }
        }
     }
});