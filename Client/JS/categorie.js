$(function() {

    $("#buttonCategorie").click(refreshCatList);

    function refreshCatList(){
        requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/categorie";
        fetch(requete)
        .then( response => {
                  if (response.ok) return response.json();
                  else throw new Error('Problème ajax: '+response.status);
                }
            )
        .then(remplirCategorie)
        .catch(onerror);
      }

    function onerror(err) {
        $("#lescategories").html("<b>Impossible de récupérer les catégories !</b>"+err);
    }

    function remplirCategorie(categorie) {
        $('#enteteCategorie').empty();
        $('#enteteCategorie').append($('<th>').text("Identifiant"));
        $('#enteteCategorie').append($('<th>').text("Nom"));
        $('#containerCategorie').empty();
        for (let i=0 ; i <categorie['categorie'].length; i++){
            $('#containerCategorie').append($('<tr id='+i+' class="table-success">'));
            $('#'+i).append($('<td>').text(categorie['categorie'][i].id));
            $('#'+i).append($('<td>').text(categorie['categorie'][i].nom));
            $('#'+i).append($('<input class="btn red" id="buttonSupCategorie" type="button" value="Supprimer"/>').on("click", categorie['categorie'][i], SuppCat));        
            $('#'+i).append($('<input class="btn red" id="buttonDetailsCategorie" type="button" value="Details"/>').on("click", categorie['categorie'][i], details));        
        }
     }

    function details(event){
        formcat();
        fillFormCat(event.data);
    }

    function formcat(){
        $("#categoriecourant").empty();
        $("#categoriecourant")
            .append($("<br>"));
            $("#categoriecourant").append($("<h1>Détails de la categorie</h1>"));
            $("#categoriecourant").append($('<span>Nom<input class="col-md-3 form-control" type="text" id="nom"></span>'));
            $("#categoriecourant").append($('<span><input  type="hidden" id="lien"><br></span>'));
    }

    function fillFormCat(t){
        $("#categoriecourant #nom").val(t.nom);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/categorie/" + t.id;
        lien=(lien == undefined)?lien: lien;
        $('#categoriecourant #identifiant').val(lien);
        $("#categoriecourant").append($('<span><input class="btn red test" type="button" value="Modifier Categorie"><br></span>').on("click", t.id, ModifierCat));;
    }

    class Categorie{
        constructor(nom){
            this.nom = nom;
            console.log(this.nom);
        }
    }

    function ModifierCat(identifiant){
        let id = JSON.stringify(identifiant["data"]);
        var cat = new Categorie(
            $("#categoriecourant #nom").val(),
            );
        console.log("PUT");
        console.log(JSON.stringify(cat));
        console.log("id : " + id);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/categorie/" + id;
        console.log("lien : " + lien);
        
        $.ajax({
            url : lien,
            type : "PUT",
            contentType : "application/json",
            data : JSON.stringify(cat),
            dataType : 'JSON',
            success : function(msg){
                alert('sauvé avec succcès');
            },
            error : function(msg){
                alert("Erreur de modification");
            }
        });

        refreshCatList();
    }

    $("#buttonAjouterCategorie").click(ajouteCategorie);

    //ajoute une categorie à la base
    function ajouteCategorie(){
        var cat = new Categorie(
            $("#nom_categorie").val()
            );
        console.log($("#nom_categorie").val());
        console.log(JSON.stringify(cat));
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/categorie/new",{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "POST",
        body : JSON.stringify(cat)
            })
        .then( res => {console.log(res);})
        .then(refreshCatList)
        .catch(res=>{console.log(res)});
    }

    function SuppCat(event){
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/categorie/" + event.data['id'],{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }, method : "DELETE"})
        .then( res=>{console.log(res); })
        .then(refreshCatList)
        .catch(res=>{console.log(res)});
    }
});