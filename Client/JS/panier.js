$(function(){

    $("#buttonPanier").click(refreshPanierList);

    function refreshPanierList(){
        requete = "http://localhost:8000/ecommerce/api/v1.0/panier/";
        fetch(requete)
        .then(response => {
            if (response.ok) return response.json();
            else throw new Error('Probleme ajax: ' + response.status); 
        }
        )
        .then(remplirPanier, )
        .catch(onerror);
    }

    function onerror( err){
        $("#taches").html("<b> Impossible de recuperer les paniers </b>" + err);
    }

    async function remplirPanier(panier){
        $('#entetePanier').empty();
        $('#entetePanier').append($('<th>').text("Identifiant"));
        $('#entetePanier').append($('<th>').text("SommeTotal"));
        $('#entetePanier').append($('<th>').text("user"));
        $('#entetePanier').append($('<th>').text("produit"));
        $('containerPanier').empty();
        for (let i=0; i <panier ['panier'].length; i++){
            $('#containerPanier').append($('<tr id='+i+' class ="table-success">'));
            $('#'+i).append($('<td>').text(panier['panier'][i].id));
            $('#'+i).append($('<td>').text(panier['panier'][i].sommeTotal));
            $('#'+i).append($('<td>').text(panier['panier'][i].user));
            $('#'+i).append($('<td>').text(panier['panier'][i].produit));
            $('#'+i).append($('<input class="red btn" id="buttonSupPanier" type="button" value="Supprimer"/>').on("click", panier['panier'][i], SuppPanier));        
            $('#'+i).append($('<input class="red btn" id="buttonDetailsPanier" type="button" value="Details"/>').on("click", panier['panier'][i], details));

        }
    }

    function details(event){
        $("panierCourant").empty();
        formPanier();
        fillFormPanier(event.data);
    }

    function formPanier(isNew){
        $("#panierCourant").empty();
        $("#panierCourant")
            .append($("<br>"))
            .append($("<h1>Détails du panier</h1>"))
            .append($('<span>SommeTotal<input class="col-md-3 form-control" type="text" id="sommeTotal"></span>'))
            .append($('<span>User<input class="col-md-3 form-control" type="text" id="user"></span>'))
            .append($('<span>Produit<input class="col-md-3 form-control" type="text" id="produit"></span>'))
            .append($('<span><input type="hidden" id="lien"><br></span>'));
    }

    function fillFormPanier(t){
        $("#panierCourant #sommeTotal").val(t.sommeTotal);
        let lien = "http://localhost:8000/ecommerce/api/v1.0/panier/" + t.id;
        lien=(lien == undefined)?lien: lien;
        $('#panierCourant #identifiant').val(lien);
        $("#panierCourant").append($('<span><input class="red btn test" type="button" value="Modifier Panier"><br></span>').on("click", t.id, ModifierPanier));
    }

    class Panier{
        constructor(sommeTotal, user, produit){
            this.sommeTotal = sommeTotal;
            this.user = user;
            this.produit = produit;
            console.log(this.sommeTotal, this.user, this.produit);
        }
    }

    function ModifierPanier(identifiant){
        let id = JSON.stringify(identifiant["data"]);
        var cat = new Panier(
            $("#panierCourant #sommeTotal").val(),
            $("#panierCourant #user").val(),
            $("#panierCourant #produit").val(),
            );
        console.log("PUT");
        console.log(JSON.stringify(panier));
        console.log("id : " + id);
        let lien = "http://localhost:8000/ecommerce/api/v1.0/panier/" + id;
        console.log("lien : " + lien);
        
        $.ajax({
            url : lien,
            type : "PUT",
            contentType : "application/json",
            data : JSON.stringify(panier),
            dataType : 'JSON',
            success : function(msg){
                alert('sauvé avec succcès');
            },
            error : function(msg){
                alert("Erreur de modification");
            }
        });
        refreshCatList();
    }

    $("#buttonAjouterPanier").click(ajoutePanier);

    async function ajoutePanier(){
        var cat = new Panier(
            $("#sommeTotal_panier").val(),
            $("#user_panier").val(),
            $("#produit_panier").val()
            );
        console.log($("#sommeTotal_panier").val());
        console.log($("#user_panier").val());
        console.log($("#produit_panier").val());
        console.log(JSON.stringify(panier));
        fetch("http://localhost:8000/ecommerce/api/v1.0/panier/new",{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "POST",
        body : JSON.stringify(panier)
            })
        .then( res => {console.log(res);})
        .then(refreshCatList)
        .catch(res=>{console.log(res)});
    }

    async function SuppPanier(event){
        fetch("http://localhost:8000/ecommerce/api/v1.0/panier/" + event.data['id'],{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }, method : "DELETE"})
        .then( res=>{console.log(res); })
        .then(refreshCatList)
        .catch(res=>{console.log(res)});
    }


});