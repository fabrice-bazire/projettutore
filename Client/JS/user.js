
$(function() {

    $("#buttonModifUser").click(modifierUser);
    $("#buttonAjoutUser").click(ajouteUser);



    function modifierUser(identifiant){
        //let id = JSON.stringify(identifiant["data"]);
        if($("#changeMdp").val()==$("#confirmMdp").val()){
            let id = $("#changeId").val;
            var user = new User(
                $("#changeNom").val(),
                $("#changePrenom").val(),
                $("#changeEmail").val(),
                $("#changeTelephone").val(),
                $("#changeMdp").val()
                );
            console.log("PUT");
            console.log(JSON.stringify(user));
            console.log("id : " + id);
            let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/user/" + id;
            console.log("lien : " + lien);
            fetch(lien,{ 
                headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'dataType' : 'JSON'
            }, method : "PUT", 
            body : JSON.stringify(user)})
            .then( res => {
                      $("#result").text(res['contenu']);
                    })
            .catch(res=>{"console.log(res)"});
        }else{
            $("#body").append($('<p>').text("Confirmation de mot de passe incorrect"));
        }
        
    }


    class User{
        constructor(nom,prenom,email,telephone,mdp){
            this.nom = nom;
            this.prenom = prenom;
            this.email = email;
            this.telephone = telephone;
            this.mdp = mdp;
            this.panier = null;
            console.log(this.nom);
        }
        getPanier(){return this.panier;}
        setPanier(panier){this.panier=panier;}
    }


    //ajoute un user à la base
    async function ajouteUser(){
        var user = new User(
            $("#nouveauNom").val(),
            $("#nouveauPrenom").val(),
            $("#nouveauEmail").val(),
            $("#nouveauTelephone").val(),
            $("#nouveauMdp").val()
            );
        console.log(JSON.stringify(user));
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/user",{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "POST", 
        body : JSON.stringify(user)})
        .then( res => {console.log(res);})
        .catch(res=>{console.log(res)});
    }





    function SuppUser(event){
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/user/" + event.data["id"],{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }, method : "DELETE"})
        .then( res=>{console.log(res);})
        .then(refreshUserList)
        .catch(res=>{console.log(res)});
    }


    /////////////////////////////////////////////////////////////

    $("#buttonListeUser").click(refreshUserList);

    function refreshUserList(){
        requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/user/";
        fetch(requete)
        .then( response => {
                  if (response.ok) return response.json();
                  else throw new Error('Problème ajax: '+response.status);
                }
            )
        .then(remplirUser)
        .catch(onerror);
      }

    function onerror(err) {
        $("#taches").html("<b>Impossible de récupérer les users !</b>"+err);
    }

    function remplirUser(users) {
        $('#enteteUser').empty();
        $('#enteteUser').append($('<th>').text("id"));
        $('#enteteUser').append($('<th>').text("Nom"));
        $('#enteteUser').append($('<th>').text("Prenom"));
        $('#enteteUser').append($('<th>').text("Email"));
        $('#enteteUser').append($('<th>').text("Telephone"));
        $('#enteteUser').append($('<th>').text("Mdp"));
        $('#containerUser').empty();
        for (let i=0;i<users['user'].length;++i){
            $('#containerUser').append($('<tr id='+i+' class="table-success">'));
            $('#'+i).append($('<td>').text(users['user'][i].id));
            $('#'+i).append($('<td>').text(users['user'][i].nom));
            $('#'+i).append($('<td>').text(users['user'][i].prenom));
            $('#'+i).append($('<td>').text(users['user'][i].email));
            $('#'+i).append($('<td>').text(users['user'][i].telephone));
            $('#'+i).append($('<td>').text(users['user'][i].mdp));
            $('#'+i).append($('<input class="btn red" id="buttonSupUsers" type="button" value="Supprimer"/>').on("click", users['user'][i], SuppUser));        
            //$('#'+i).append($('<input id="buttonDetailsProduits" type="button" value="Details"/>').on("click", produits['produit'][i], details));        
        }
     }

     /*function details(event){
        $("#produitcourant").empty();
        formprod();
        fillFormTask(event.data);
    }*/

    function formuser(isnew){
        $("#usercourant").empty();
        $("#usercourant")
            .append($("<br>"))
            .append($("<h1>Détails du user</h1>"))
            .append($('<span>id<input class="col-md-3 form-control" type="text" id="id"></span>'))
            .append($('<span>Nom<input class="col-md-3 form-control" type="text" id="nom"></span>'))
            .append($('<span>Prenom<input class="col-md-3 form-control" type="text" id="prenom"></span>'))
            .append($('<span>Email<input class="col-md-3 form-control" type="text" id="email"></span>'))
            .append($('<span>Telephone<input class="col-md-3 form-control" type="text" id="telephone"></span>'))
            .append($('<span>Mdp<input class="col-md-3 form-control" type="text" id="mdp"></span>'))
            .append($('<span><input type="hidden" id="lien"><br></span>'));
    }

    function fillFormTask(t){
        $("#usercourant #id").val(t.id);
        $("#usercourant #nom").val(t.nom);
        $("#usercourant #prenom").val(t.prenom); //JQUERY ou VanillaJS : document.getElementById('descr').value = t.description;
        $("#usercourant #email").val(t.email);
        $("#usercourant #telephone").val(t.telephone);
        $("#usercourant #mdp").val(t.mdp);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0user/" + t.id;
        lien=(lien == undefined)?lien: lien;
        $('#usercourant #identifiant').val(lien);
        $("#usercourant").append($('<span><input class="btn red test" type="button" value="Modifier user"><br></span>').on("click", t.id, modifierUser));;
    }
});