$(function() {

    $("#buttonProduits").click(refreshProdList);

    function refreshProdList(){
        requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/produit";
        fetch(requete)
        .then( response => {
                  if (response.ok) return response.json();
                  else throw new Error('Problème ajax: '+response.status);
                }
            )
        .then(remplirProduits)
        .catch(onerror);
      }

    function onerror(err) {
        $("#lesproduits").html("<b>Impossible de récupérer les produits !</b>"+err);
    }

    function remplirProduits(produits) {
        $('#enteteProduit').empty();
        $('#enteteProduit').append($('<th>').text("Identifiant"));
        $('#enteteProduit').append($('<th>').text("Nom"));
        $('#enteteProduit').append($('<th>').text("Ref"));
        $('#enteteProduit').append($('<th>').text("Prix"));
        // $('#enteteProduit').append($('<th>').text("Categorie"));
        // $('#enteteProduit').append($('<th>').text("Marque"));
        $('#containerProduit').empty();
        for (let i=0;i<produits['produit'].length;++i){
            $('#containerProduit').append($('<tr id='+i+' class="table-success">'));
            $('#'+i).append($('<td>').text(produits['produit'][i].id));
            $('#'+i).append($('<td>').text(produits['produit'][i].nom));
            $('#'+i).append($('<td>').text(produits['produit'][i].ref));
            $('#'+i).append($('<td>').text(produits['produit'][i].prix));
            // $('#'+i).append($('<td>').text(produits['produit'][i].categorie));
            // $('#'+i).append($('<td>').text(produits['produit'][i].marque));
            $('#'+i).append($('<input class="btn red" id="buttonSupProduits" type="button" value="Supprimer"/>').on("click", produits['produit'][i], SuppProd));        
            $('#'+i).append($('<input class="btn red" id="buttonDetailsProduits" type="button" value="Details"/>').on("click", produits['produit'][i], details));        
        }
     }

    //  function refreshMarqueList(){
    //     requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/marque";
    //     fetch(requete)
    //     .then( response => {
    //               if (response.ok) return response.json();
    //               else throw new Error('Problème ajax: '+response.status);
    //             }
    //         )
    //     .then(remplirMarque)
    //     .catch(onerror);
    //   }

    // function onerror(err) {
    //     $("#lescategories").html("<b>Impossible de récupérer les marques !</b>"+err);
    // }

    // function remplirMarque(marque) {
    //     $('#lesmarques').empty();
    //     for (let i=0 ; i <marque['marque'].length; i++){
    //         $('#lesmarques').append($('<option>').text(marque['marque'][i].nom).val(marque['marque'][i])); 
    //     }
    //  }

    //  function refreshCategorieList(){
    //     requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/categorie";
    //     fetch(requete)
    //     .then( response => {
    //               if (response.ok) return response.json();
    //               else throw new Error('Problème ajax: '+response.status);
    //             }
    //         )
    //     .then(remplirCategorie)
    //     .catch(onerror);
    //   }

    // function onerror(err) {
    //     $("#lescategories").html("<b>Impossible de récupérer les categories !</b>"+err);
    // }

    // function remplirCategorie(categorie) {
    //     $('#lescategories').empty();
    //     for (let i=0 ; i <categorie['categorie'].length; i++){
    //         $('#lescategories').append($('<option>').text(categorie['categorie'][i].nom).val(categorie['categorie'][i]));   
    //     }
    //  }

     function details(event){
        formprod();
        fillFormProd(event.data);
    }

    // window.onload = refreshMarqueList();
    // window.onload = refreshCategorieList();

    function formprod(){
        $("#produitcourant").empty();
        $("#produitcourant")
            .append($("<br>"))
            .append($("<h1>Détails du produit</h1>"))
            .append($('<span>Nom<input class="col-md-3 form-control" type="text" id="nom"></span>'))
            .append($('<span>Reference<input class="col-md-3 form-control" type="text" id="ref"></span>'))
            .append($('<span>Prix<input class="col-md-3 form-control" type="text" id="prix"></span>'))
            // .append($('<span>Categorie<input type="text" id="categorie"><br></span>'))
            // .append($('<span>Marque<input type="text" id="marque"><br></span>'))
            .append($('<span><input type="hidden" id="lien"><br></span>'));
    }

    function fillFormProd(t){
        $("#produitcourant #nom").val(t.nom);
        $("#produitcourant #ref").val(t.ref); //JQUERY ou VanillaJS : document.getElementById('descr').value = t.description;
        $("#produitcourant #prix").val(t.prix);
        // $("#produitcourant #categorie").val(t.categorie);
        // $("#produitcourant #marque").val(t.marque);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/produit/" + t.id;
        lien=(lien == undefined)?lien: lien;
        $('#produitcourant #identifiant').val(lien);
        $("#produitcourant").append($('<span><input class="btn red test" type="button" value="Modifier produit"><br></span>').on("click", t.id, ModifierProduit));;
    }

    class Produit{
        constructor(nom, ref, prix, categorie, marque){
            this.nom = nom;
            this.ref = ref;
            this.prix = prix;
            this.categorie = categorie;
            this.marque = marque;
        }
    }

    function ModifierProduit(identifiant){
        let id = JSON.stringify(identifiant["data"]);
        var prod = new Produit(
            $("#produitcourant #nom").val(),
            $("#produitcourant #ref").val(),
            $("#produitcourant #prix").val(),
            null,
            null
            // $("#produitcourant #categorie").val(),
            // $("#produitcourant #marque").val()
            );
        console.log("PUT");
        console.log(JSON.stringify(prod));
        console.log("id : " + id);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/produit/" + id;
        console.log("lien : " + lien);
        fetch(lien,{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "PUT", 
        body : JSON.stringify(prod)})
        .then( res => {
                  $("#result").text(res['contenu']);
                  refreshProdList();
                })
        .catch(res=>{console.log(res)});
    }

    $("#buttonAjouterProduit").click(ajouteProduit);

    //ajoute un produit à la base
    function ajouteProduit(){
        var prod = new Produit(
            $("#nom_produit").val(),
            $("#ref_produit").val(),
            $("#prix_produit").val(),
            null,
            null
            // $("#lescategories option:selected").val(),
            // $("#lesmarques option:selected").val()
            );
        console.log(JSON.stringify(prod));
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/produit",{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "POST", 
        body : JSON.stringify(prod)})
        .then( res => {console.log(res);})
        .then(refreshProdList)
        .catch(res=>{console.log(res)});
    }

    function SuppProd(event){
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/produit/" + event.data["id"],{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }, method : "DELETE"})
        .then( res=>{console.log(res);})
        .then(refreshProdList)
        .catch(res=>{console.log(res)});
    }
});