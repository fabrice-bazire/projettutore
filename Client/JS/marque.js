$(function() {

    $("#buttonMarques").click(refreshMarqueList);

    function refreshMarqueList(){
        requete = "http://127.0.0.1:8000/ecommerce/api/v1.0/marque";
        fetch(requete)
        .then( response => {
                  if (response.ok) return response.json();
                  else throw new Error('Problème ajax: '+response.status);
                }
            )
        .then(remplirMarque)
        .catch(onerror);
      }

    function onerror(err) {
        $("#lescategories").html("<b>Impossible de récupérer les marques !</b>"+err);
    }

    function remplirMarque(marque) {
        $('#enteteMarques').empty();
        $('#enteteMarques').append($('<th>').text("Identifiant"));
        $('#enteteMarques').append($('<th>').text("Nom"));
        $('#containerMarque').empty();
        for (let i=0 ; i <marque['marque'].length; i++){
            $('#containerMarque').append($('<tr id='+i+' class="table-success">'));
            $('#'+i).append($('<td>').text(marque['marque'][i].id));
            $('#'+i).append($('<td>').text(marque['marque'][i].nom));
            $('#'+i).append($('<input class="btn red" id="buttonSupMarque" type="button" value="Supprimer"/>').on("click", marque['marque'][i], SuppMarque));        
            $('#'+i).append($('<input class="btn red" id="buttonDetailsMarque" type="button" value="Details"/>').on("click", marque['marque'][i], details));        
        }
     }

    function details(event){
        formmarque();
        fillFormMarque(event.data);
    }

    function formmarque(){
        $("#marquecourante").empty();
        $("#marquecourante")
            .append($("<br>"));
            $("#marquecourante").append($("<h1>Détails de la Marque</h1>"));
            $("#marquecourante").append($('<span>Nom<input class="col-md-3 form-control" type="text" id="nom"></span>'));
            $("#marquecourante").append($('<span><input type="hidden" id="lien"><br></span>'));
    }

    function fillFormMarque(t){
        $("#marquecourante #nom").val(t.nom);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/marque/" + t.id;
        lien=(lien == undefined)?lien: lien;
        $('#marquecourante #identifiant').val(lien);
        $("#marquecourante").append($('<span><input class="btn red test" type="button" value="Modifier Marque"><br></span>').on("click", t.id, ModifierMarque));;
    }

    class Marque{
        constructor(nom){
            this.nom = nom;
            console.log(this.nom);
        }
    }

    function ModifierMarque(identifiant){
        let id = JSON.stringify(identifiant["data"]);
        var marque = new Marque(
            $("#marquecourante #nom").val(),
            );
        console.log("PUT");
        console.log(JSON.stringify(marque));
        console.log("id : " + id);
        let lien = "http://127.0.0.1:8000/ecommerce/api/v1.0/marque/" + id;
        console.log("lien : " + lien);
        fetch(lien,{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "PUT", 
        body : JSON.stringify(marque)})
        .then( res => {
                  console.log(res);
                  refreshMarqueList();
                })
        .catch(res=>{console.log(res)});
    }

    $("#buttonAjouterMarque").click(ajouteMarque);

    //ajoute une categorie à la base
    function ajouteMarque(){
        var marque = new Marque(
            $("#nom_marque").val()
            );
        console.log($("#nom_marque").val());
        console.log(JSON.stringify(marque));
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/marque",{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }, method : "POST",
        body : JSON.stringify(marque)
            })
        .then( res => {console.log(res);})
        .then(refreshMarqueList)
        .catch(res=>{console.log(res)});
    }

    function SuppMarque(event){
        fetch("http://127.0.0.1:8000/ecommerce/api/v1.0/marque/" + event.data['id'],{ 
            headers : {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }, method : "DELETE"})
        .then( res=>{console.log(res); })
        .then(refreshMarqueList)
        .catch(res=>{console.log(res)});
    }
});