# Projet Tutore
Site de E-Commerce

## Installation du site

- Premierement, télécharger [l'archive](https://gitlab.com/fabrice-bazire/projettutore/-/blob/master/Site-Final.zip) du site.
- Décompressez là dans le dossier de votre choix et ouvrez un terminal dans le dossier Serveur
- Effectuez une migration (deux commandes suivantes) afin de mettre à jour la base de données
```bash
php bin/console make:migration
```
```bash
php bin/console doctrine:migrations:migrate
```
- L'installation du site est terminée

## Utilisation
Il vous faut avant toute chose lancé le serveur avec lequel votre site va communiquer (toujours dans un terminal a la racine du dossier Serveur) : 
```bash
symfony server:start
```
Ensuite, vous pouvez aller dans le dossier client et ouvrir le fichier connexion.html dans votre navigateur web.  
Vous arrivez alors sur la page d'accès au site.

## Ce qui fonctionne
- La page de connexion fonctionne, on peut bel et bien se connecter sur le site seulement si on entre les bons identifiants
- La page produit est aussi fonctionnelle, tout les boutons et les fonctionnalités de cette page sont ok.
- La page user est quasi fonctionnelle, seulement la modification d'un user pose encore problème.
- La page panier est elle seulement en partie fonctionnelle, on ne peut pas ajouter, supprimer ou modifier un panier.  
 On peut seulement afficher les paniers (un panier en détail ou la liste des paniers)
- Le design du site fonctionne via bootstrap.

## Améliorations à apporter
Par soucis de temps, nous n'avons pas eu le temps de finir le projet comme nous l'aurions souhaiter. 
  
Nous avions prévu de mettre en place une interface reservée à l'administrateur pour qu'il puisse gérer le site ainsi qu'une interface destinée à l'utilisateur qui lui a accès à la gestion de son profil et a la consultation des produits.
  
Nous aurions voulu mettre en place aussi des images sur les produits comme on peut le voir sur les maquettes.  

Nous aurions voulu finaliser en corrigeant tout les bugs restants (certaines fonctions non fonctionnelles, certains boutons non cliquables, gestion des relations non aboutie)  

Voilà, je pense qu'une fois tout ça ensemble on aurait eu un site plutôt complet.

## Remerciements
Merci à Mr Rozavolgyi, Mr Austry et Mr Arsouze pour leur aide tout au sein de ce projet et aux débeugages apportés.  

## Source
Vous pouvez retrouver l'ensemble du code [ici](https://gitlab.com/fabrice-bazire/projettutore).

  
### Groupe ASB
Jayhson, Aaron, Baptiste, Alexandre, Maël et Fabrice